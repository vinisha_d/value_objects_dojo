import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class EuroTest {

    @Test
    public void checkIfTwoSameEuroValuesAreEqual() {
        assertEquals(new Euro(10), new Euro(10));
    }

    @Test
    public void checkIfTwoDifferentEuroValuesAreNotEqual() {
        assertNotEquals(new Euro(10), new Euro(5));
    }

    @Test
    public void checkIfEuroIsNotEqualToNull() {
        assertNotEquals(new Euro(10), null);
    }

    @Test
    public void checkIfEuroIsNotEqualToOtherObject() {
        assertNotEquals(new Object(), new Euro(10));
    }

    @Test
    void checkIf10EuroIsEqualTo7EuroPlus3Euro() {
        assertEquals(new Euro(10), new Euro(7).add(new Euro(3)));
    }

    @Test
    void checkIf10EuroIsNotEqualTo5EuroPlus2Euro() {
        assertNotEquals(new Euro(10), new Euro(5).add(new Euro(2)));
    }
}
